# Backslide Presentations Template

This is a simple template to demonstrate gitlab pages using [backslide](https://github.com/sinedied/backslide) for making HTML presentations with [Remark.js](https://github.com/gnab/remark) using [Markdown](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet).

This example loads the file `presenatation.md` and creates a presentation in HTML and PDF format that are available on GitLab Pages:
* [HTML](https://fdm-nrw-gitlabexamples.gitlab.io/backslidepresentations/presentation.html#1)
* [PDF](https://fdm-nrw-gitlabexamples.gitlab.io/backslidepresentations/presentation.pdf)

## Quickstart

1. Create a fork of this repository or copy all the files to a new repository
1. Add a license of your choosing e.g. see https://choosealicense.com/non-software/
    * Update `LICENSE` file
1. Add your content to `presentation.md`
1. If you want to add other presentations, copy/change the scripts in `.gitlab-ci.yml`

After pushing to the `main` branch, your presentation will be automatically updated and uploaded.

## How This Works in Detail

This repository uses the GitLab Pages feature to serve presentation slides in HTML and PDF format.

## About Backslide and Remark.js

Backslide is a small tool to convert markdown files to HTML presentations. The Markdown is rendered in the browser by Remark.js. In this repository Backslide is loaded using the docker image [`registry.gitlab.com/mpolitze/backslide-docker:latest`](https://gitlab.com/mpolitze/backslide-docker). See the CI job `backslide` for details on how this works.

Backslide loads a markdown file, in this case `presentation.md` and converts it into a stand alone HTML file that includes the JavaScript needed to render the markdown as slides in a browser. Backslide useses Remark.js for that. For the conversion Backslide uses some boilerplate files in the `template` folder that can be customized to change the look of your presentation. You can look at `presentation.md` for some examples or refer to the wiki of Remark.js on how to author your slides: https://github.com/gnab/remark/wiki.

## About Decktape

Decktape is a PDF converter for HTML based slides. It simulates a browser and "prints" the slides to PDF. Again we are using a docker image to load decktape in the workflow: [`astefanutti/decktape`](https://hub.docker.com/r/astefanutti/decktape).

Decktape loads the presentation from the HTML file and converts it into a PDF file. See the CI job `decktape` for details on how this works.

## About GitLab Pages

GitLab Pages is a feature of GitLab that allows to publish static files as web pages served by GitLab itself - no additional hosting is needed. 

To enable GitLab Pages you need a GitLab CI job that is called `pages`. Everytime this job is run, it will pick up the contents of the folder `public` within the jobs artifacts and serve those as static files on the webserver. You can create the contents of the `public` folder in the CI job but it can also be files that reside in the `public` folder of your repository. 

## Putting Things Together

To serve our `presentation.md` as HTML and PDF from the GitLab Pages server, we first need to convert that file to a HTML file in `public/presentation.html` during the CI job `backslide`. The generated file is temporarily saved as artifact for the next CI job. The `decktape` Job picks up the HTML from the artifact, creates a PDF file that is again stored as an artifact. Finally The GitLab CI script of the `pages` job is just there to indicate that the `public` folder (including the genrated artifacts) can be picked up by GitLab Pages.

## Extending for more presentations

If you want to have multiple presentations in one git repository you have the following options:

1. Replicate the calls to Dackslide and Decktape replacing the presentation file (`presentation.md`, `presentation.html`, `presentation.pdf`) with the respective names. -OR-
1. If you have more presentations and you do not want to manually enumerate them in the `.gitlab-ci.yml` file. Prepend a `find` command to list all files like so:
  * For Backslide use `bs export --output ./public` to run for all `*.md` files or use `find` fore more fine granular control like this `find *.md | xargs -i -P2  bs export "{}" --output ./public` 
  * For Decktape use `find public/*.html -exec basename -s .html "{}" \; | xargs -i -P2 node /decktape/decktape.js --chrome-path chromium-browser --chrome-arg=--no-sandbox "{}.html" "{}.pdf"`

  For more complex folder structures you might have to change the find command accordingly.
